package com.example.shatera.mycheckboxes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private CheckBox BKchk;
    private CheckBox SSchk;
    private CheckBox Wendyschk;
    private Button Submitbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addListenerBK();
        addListenerSSchk();
        addListenerWendys();
        Submitbtn=(Button)findViewById(R.id.btnSubmit);
        Submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer result = new StringBuffer();
                result.append("BurgerKing :" ).append(BKchk.isChecked());
                result.append("\n ShakeShack :" ).append(SSchk.isChecked());
                result.append("\n Wendy's :" ).append(Wendyschk.isChecked());
                Toast.makeText(MainActivity.this, result.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void addListenerBK(){
        BKchk=(CheckBox)findViewById(R.id.chkBK);
        BKchk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()){
                    Toast.makeText(MainActivity.this, "BugerKing is selected", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void addListenerSSchk(){
        SSchk=(CheckBox)findViewById(R.id.chkSS);
        SSchk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    Toast.makeText(MainActivity.this, "ShakeShack is selcted ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public void addListenerWendys(){
        Wendyschk=(CheckBox)findViewById(R.id.chkWendys);
        Wendyschk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox)v).isChecked()){
                    Toast.makeText(MainActivity.this, "Wendy's is selected", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
